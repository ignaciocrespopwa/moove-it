import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import { TabNavigator, StackNavigator } from "react-navigation";

import AllMovieScreen from "./screens/AllMovieScreen";
import DetailsScreen from "./screens/DetailsScreen";
import ReviewsScreen from "./screens/ReviewScreen";
export default class AllBeersScreen extends Component {
  render() {
    const MainNavigator = StackNavigator(
      {
        allmovie: { screen: AllMovieScreen },
        details: { screen: DetailsScreen },
        reviews: { screen: ReviewsScreen }
      },
      {
        headerMode: "none",
        navigationOptions: {
          tabBarVisible: false,
          headerVisible: false
        },
        lazyLoad: true
      }
    );

    return <MainNavigator />;
  }
}
