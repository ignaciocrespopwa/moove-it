import React, { Component } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  View,
  Dimensions,
  ActivityIndicator,
  TouchableOpacity
} from "react-native";
import axios from "axios";

import CardComponent from "./CardComponent";

export default class FlatListInfinit extends Component {
  state = {
    data: [],
    page: 1,
    loading: true,
    loadingMore: false,
    error: null,
    refreshing: false
  };

  componentDidMount() {
    this._fetchAllBeers();
  }

  _fetchAllBeers = () => {
    const { page } = this.state;
    const URL = `page=${page}`;
    axios
      .get(
        `https://api.themoviedb.org/3/discover/movie?api_key=f2e9d2d3496ccdab911fbc04867aa2e0&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${page}`
      )
      .then(response => {
        this.setState((prevState, nextProps) => ({
          data:
            page === 1
              ? Array.from(response.data.results)
              : [...this.state.data, ...response.data.results],
          loading: false
        }));
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  _renderFooter = () => {
    if (!this.state.loadingMore) return null;
    return (
      <View
        style={{
          position: "relative",
          width: "50%",
          height: "50%",
          paddingVertical: 20,
          borderTopWidth: 1,
          marginTop: 10,
          marginBottom: 10,
          borderColor: "black"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this._fetchAllBeers();
      }
    );
  };

  _handleLoadMore = () => {
    this.setState(
      (prevState, nextProps) => ({
        page: prevState.page + 1,
        loadingMore: true
      }),
      () => {
        this._fetchAllBeers();
      }
    );
  };

  render() {
    return (
      <FlatList
        style={styles.flex}
        contentContainerStyle={{
          flexDirection: "column",
          width: "100%"
        }}
        data={this.state.data}
        renderItem={({ item }) => <CardComponent itemRender={item} />}
        onEndReached={this._handleLoadMore}
        ListFooterComponent={this._renderFooter}
        onEndReachedThreshold={0.5}
        initialNumToRender={5}
        onRefresh={this._handleRefresh}
        refreshing={this.state.refreshing}
        numColumns={2}
        keyExtractor={(item, index) => index}
      />
    );
  }
}

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    marginTop: 100,
    backgroundColor: "white"
  }
});
