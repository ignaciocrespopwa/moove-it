import React, { Component } from "react";
import glamorous from "glamorous-native";
import { View, Text, ImageBackground } from "react-native";

const CardContainer = glamorous.view((props, theme) => ({
  height: 160,
  width: "60%",
  left: "20%",
  justifyContent: "space-around",
  margin: 5
}));

class CardComponent extends Component {
  render() {
    return (
      <CardContainer>
        <ImageBackground
          source={{
            uri: `https://image.tmdb.org/t/p/w500/${
              this.props.itemRender.poster_path
            }`
          }}
          style={styles.backgroundImage}
        >
          <View style={styles.slideStyle}>
            <View style={styles.voteIndicator}>
              <Text style={styles.textVote}>
                {this.props.itemRender.vote_average}
              </Text>
            </View>
            <Text style={styles.textTitle}>{this.props.itemRender.title}</Text>
          </View>
        </ImageBackground>
      </CardContainer>
    );
  }
}

const styles = {
  backgroundImage: {
    flex: 1,
    width: "100%",
    height: "100%",
    resizeMode: "cover",
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,
    overflow: "hidden"
  },
  textTitle: {
    color: "white",
    position: "absolute",
    bottom: 0,
    fontSize: 10,
    backgroundColor: "black"
  },
  textVote: {
    color: "white",
    fontSize: 13
  },
  voteIndicator: {
    width: 20,
    height: 20,
    float: "right",
    top: 0,
    textAlign: "center",
    backgroundColor: "orange",
    position: "absolute",
    backgroundColor: "orange",
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
    overflow: "hidden"
  },
  slideStyle: {
    flex: 1,
    width: "50%",
    height: 200
  }
};

export default CardComponent;
