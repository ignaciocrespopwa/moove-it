import React, { Component } from "react";
import glamorous from "glamorous-native";
import { View, Text, Image } from "react-native";

class CardMoviesInfo extends Component {
  _genres() {
    return "Comedy";
  }

  _voteAverage() {
    return 5;
  }

  render() {
    return (
      <View style={styles.containerCard}>
        <Image
          source={{
            uri: `https://image.tmdb.org/t/p/w500/${
              this.props.itemRender.poster_path
            }`
          }}
          style={styles.backgroundImage}
        />
        <View style={styles.infoRow}>
          <Text>{this.props.itemRender.title}</Text>
          <Text>{this.props.itemRender.popularity} People wathing</Text>
          <Text>{this._genres}</Text>
          <Text>{this._voteAverage}</Text>
        </View>
        <Text style={styles.description}>{this.props.itemRender.overview}</Text>
      </View>
    );
  }
}

const styles = {
  containerCard: {
    height: 600,
    margin: 10
  },
  infoRow: {
    position: "absolute",
    right: 0,
    width: "50%"
  },
  description: {
    top: 350
  },
  backgroundImage: {
    flex: 1,
    width: "40%",
    height: "40%",
    resizeMode: "cover",
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,
    position: "absolute",
    overflow: "hidden"
  },
  textTitle: {
    color: "white",
    position: "absolute",
    bottom: 0,
    fontSize: 10,
    backgroundColor: "black"
  }
};

export default CardMoviesInfo;
