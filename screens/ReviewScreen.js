import React, { Component } from "react";
import { View, Text, Button, StyleSheet } from "react-native";
import { StackNavigator } from "react-navigation";
import axios from "axios";

import CardMoviesInfo from "../components/CardMoviesInfo";

export default class ReviewsScreen extends Component {
  componentDidMount() {
    this._thisFetchReviews();
  }

  _thisFetchReviews = () => {
    const { navigation } = this.props;
    const itemId = navigation.getParam("id");
    axios
      .get(
        `https://api.themoviedb.org/3/movie/${itemId}/reviews?api_key=f2e9d2d3496ccdab911fbc04867aa2e0&language=en-US&page=1`
      )
      .then(response => {
        this.setState((prevState, nextProps) => ({
          data: response.data
        }));
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>Hola reviews</Text>
        <Button
          title="Go back"
          onPress={() => this.props.navigation.goBack()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: 50
  }
});
