import React, { Component } from "react";
import { View, Text, Button, StyleSheet } from "react-native";
import { StackNavigator } from "react-navigation";
import axios from "axios";

import CardMoviesInfo from "../components/CardMoviesInfo";

export default class DetailsScreen extends Component {
  state = {
    data: {}
  };

  componentDidMount() {
    this._thisFetchDetails();
  }

  _thisFetchDetails = () => {
    const { navigation } = this.props;
    const itemId = navigation.getParam("itemId");
    axios
      .get(
        `https://api.themoviedb.org/3/movie/${itemId}?api_key=f2e9d2d3496ccdab911fbc04867aa2e0&language=en-US`
      )
      .then(response => {
        this.setState((prevState, nextProps) => ({
          data: response.data
        }));
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <Button
          title="Go back"
          onPress={() => this.props.navigation.goBack()}
        />
        <CardMoviesInfo itemRender={this.state.data} />
        <Button
          title="View Reviews"
          onPress={() =>
            this.props.navigation.push("reviews", { id: this.state.id })
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: 50
  }
});
